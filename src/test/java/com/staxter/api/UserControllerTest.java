package com.staxter.api;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.staxter.TestApplication;
import com.staxter.service.UserService;
import com.staxter.utils.JsonUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.staxter.util.UserFields.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author daniel
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
@WebAppConfiguration
public class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void post_should_register_user() throws Exception {

        JsonNode registrationData = userRegistrationData();
        mockMvc.perform(
                post(UserController.ENDPOINT)
                        .content(JsonUtil.toJson(registrationData))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.firstName", is(registrationData.get(firstName.toString()).asText())))
                .andExpect(jsonPath("$.lastName", is(registrationData.get(lastName.toString()).asText())))
                .andExpect(jsonPath("$.userName", is(registrationData.get(userName.toString()).asText())));
    }

    @Test
    public void should_return_bad_request_when_post_invalid_data() throws Exception {

        JsonNode registrationData = userRegistrationData();
        ((ObjectNode)registrationData).put(userName.toString(), "User name");
        userService.createUser(registrationData);

        mockMvc.perform(
                post(UserController.ENDPOINT)
                        .content(JsonUtil.toJson(registrationData))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isConflict())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", notNullValue()))
                .andExpect(jsonPath("$.description", notNullValue()));
    }


    private JsonNode userRegistrationData() {

        ObjectNode userRegistrationData = new ObjectMapper().createObjectNode();
        userRegistrationData.put(firstName.toString(), "Some first name");
        userRegistrationData.put(lastName.toString(), "The last name");
        userRegistrationData.put(userName.toString(), "The user name");
        userRegistrationData.put(password.toString(), "The password in plain text");


        return userRegistrationData;
    }

}
