package com.staxter.api;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.staxter.service.UserService;
import com.staxter.util.UserFields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.UUID;


import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author daniel
 */
@RestController
@RequestMapping(UserController.ENDPOINT)
public class UserController {

    public static final String ENDPOINT = "/userservice/register";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserService userService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonNode> registerUser(@RequestBody JsonNode userDto) {
        logger.info("'registerUser' invoked with param: '{}'", userDto);

        JsonNode registered = userService.createUser(userDto);
        ResponseEntity response = ResponseEntity.ok().body(registered);
        System.out.println(registered);
        logger.info("'registerUser({})' returned '{}'", userDto, response);
        return response;
    }
}
