package com.staxter.converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.staxter.entity.User;
import com.staxter.util.UserFields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.staxter.util.UserFields.*;

/**
 * @author daniel
 */
@Component("jsonNodeToUser")
public class JsonNodeToUser implements Converter<JsonNode, User>{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public User convert(JsonNode userData) {
        logger.info("'convert' invoked with param: '{}'", userData);

        if (userData == null) {
            throw new IllegalArgumentException("User data can not be null");
        }

        User user = new User();
        if (userData.has(id.toString()) && !userData.get(id.toString()).isNull()) {
            user.setId(userData.get(UserFields.id.toString()).asText());
        }
        if (userData.has(firstName.toString()) && !userData.get(firstName.toString()).isNull()) {
            user.setFirstName(userData.get(firstName.toString()).asText());
        }
        if (userData.has(lastName.toString()) && !userData.get(lastName.toString()).isNull()) {
            user.setLastName(userData.get(lastName.toString()).asText());
        }
        if (userData.has(userName.toString()) && !userData.get(userName.toString()).isNull()) {
            user.setUserName(userData.get(userName.toString()).asText());
        }
        if (userData.has(password.toString()) && !userData.get(password.toString()).isNull()) {
            user.setPlainTextPassword(userData.get(password.toString()).asText());
        }

        logger.info("'convert({})' returned '{}'", userData, user);
        return user;
    }

    @Component("userToJsonNode")
    public static class UserToJsonNode implements Converter<User, JsonNode> {

        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        @Override
        public JsonNode convert(User user) {
            logger.info("'convert' invoked with param: '{}'", user);

            if (user == null) {
                throw new IllegalArgumentException("User can not be null");
            }
            ObjectNode userData = new ObjectMapper().createObjectNode();
            userData.put(id.toString(), "Id generated by the back-end");
            userData.put(firstName.toString(), user.getFirstName());
            userData.put(lastName.toString(), user.getLastName());
            userData.put(userName.toString(), user.getUserName());

            logger.info("'convert({})' returned '{}'", user, userData);
            return userData;
        }
    }
}
