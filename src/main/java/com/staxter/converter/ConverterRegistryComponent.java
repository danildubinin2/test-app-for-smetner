package com.staxter.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author daniel
 */
@Component("converterRegistryComponent")
public class ConverterRegistryComponent {

    @Autowired
    @Qualifier("conversionService")
    private ConversionServiceFactoryBean conversionServiceFactoryBean;

    @Autowired
    @Qualifier("jsonNodeToUser")
    private Converter jsonNodeToUser;

    @Autowired
    @Qualifier("userToJsonNode")
    private Converter userToJsonNode;

    @PostConstruct
    private void init() {
        ConversionService conversionService = conversionServiceFactoryBean.getObject();
        ConverterRegistry registry = (ConverterRegistry) conversionService;

        registry.addConverter(this.jsonNodeToUser);
        registry.addConverter(this.userToJsonNode);
    }
}
