package com.staxter.converter;

import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.stereotype.Service;

/**
 * @author daniel
 */
@Service("conversionService")
public class ConversionServiceFactory extends ConversionServiceFactoryBean {
}
