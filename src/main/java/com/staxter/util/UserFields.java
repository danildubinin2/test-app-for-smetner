package com.staxter.util;

/**
 * @author daniel
 */
public enum UserFields {
    id,
    firstName,
    lastName,
    userName,
    password;

}
