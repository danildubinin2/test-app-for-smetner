package com.staxter.repository.impl;

import com.staxter.entity.User;
import com.staxter.exception.UserAlreadyExistsException;
import com.staxter.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daniel
 */
@Service
public class UserRepositoryImpl implements UserRepository {

    private List<User> users = new ArrayList<>();
    private Long idCounter = 1L;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public User createUser(User user) throws UserAlreadyExistsException {
        logger.info("'createUser' invoked with param:{}", user);
        if(isExist(user.getUserName())){
            throw new UserAlreadyExistsException("A user with the given username already exists");
        }
        user.setId(String.valueOf(idCounter++));
        users.add(user);
        logger.info("'createUser({})' returned:{}", user, user);
        return user;
    }

    @Override
    public boolean isExist(String userName) {
        return users.stream().filter(e -> e.getUserName().equals(userName)).findFirst().orElse(null) != null;
    }

}
