package com.staxter.repository;

import com.staxter.entity.User;
import com.staxter.exception.UserAlreadyExistsException;

/**
 * @author daniel
 */
public interface UserRepository {
    User createUser(User user) throws UserAlreadyExistsException;

    boolean isExist(String userName);
}
