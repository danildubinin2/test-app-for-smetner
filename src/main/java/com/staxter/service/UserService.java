package com.staxter.service;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author daniel
 */
public interface UserService {
    JsonNode createUser(JsonNode userData);
}
