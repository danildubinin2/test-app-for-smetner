package com.staxter.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.staxter.entity.User;
import com.staxter.repository.UserRepository;
import com.staxter.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author daniel
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ConversionService conversionService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,
                           @Qualifier("conversionService")ConversionService conversionService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.conversionService = conversionService;
    }

    @Override
    public JsonNode createUser(JsonNode userData) {
        logger.info("'createUser' invoked with param: {}", userData);
        User registered = conversionService.convert(userData, User.class);
        registered.setHashedPassword(passwordEncoder.encode(registered.getPlainTextPassword()));
        userRepository.createUser(registered);
        JsonNode registeredData = conversionService.convert(registered, JsonNode.class);
        logger.info("'createUser({})' returned :{}", userData, registeredData);
        return registeredData;
    }
}
