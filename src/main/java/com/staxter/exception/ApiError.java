package com.staxter.exception;

/**
 * @author daniel
 */
public final class ApiError {

    private String code;
    private String description;

    public ApiError(String code) {
        this.code = code;
    }

    public ApiError(String code, String description) {
        this(code);
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
