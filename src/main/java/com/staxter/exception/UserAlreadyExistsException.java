package com.staxter.exception;

/**
 * @author daniel
 */
public class UserAlreadyExistsException extends RuntimeException {

    private String description;
    private final static String CODE = "USER_ALREADY_EXISTS";


    public UserAlreadyExistsException() {
        super(CODE);
    }

    public UserAlreadyExistsException(String error) {
        super(CODE);
        setDescription(error);
    }

    public void setDescription(String error) {
        this.description = error;
    }

    public String getError() {

        return (description != null) ? description : null;
    }

    @Override
    public String getMessage() {

        return super.getMessage();
    }
}
